import Vue from 'vue'

import App from './App'
import BootstrapVue from 'bootstrap-vue'
import router from './router'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import GSignInButton from 'vue-google-signin-button'
import VueResource from 'vue-resource'

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(GSignInButton)
Vue.use(VueResource)

Vue.http.interceptors.push(function (request, next) {
  let token = localStorage.getItem('JWT')
  if (token != null) {
    request.headers.set('Authorization', 'Bearer ' + token)
    request.headers.set('Accept', 'application/json')
  }
  next()
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
